GAPPS for Oneplus 8 Series
----------------------------

** To be used for inline building of OnePlus 8 Series devices

** Almost all  apps are extracted from latest Oneplus 8 Pro dumps
located on: https://git.rip/dumps/oneplus/oneplus8pro

** SetupWizard and MarkupGoogle prebuilts are from latest redfin dump


How to include to your builds:

1. Sync this repo to vendor/gapps
2. Sync also https://github.com/semdoc/vendor_extra to vendor/extra
3. include these gapps to your builds:

   $ export WITH_GMS=true

4. Enjoy !!


